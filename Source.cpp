#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <stdlib.h>
using namespace std;

struct pdaStackCell{
	char symb;
	int state;
};

struct pdaStack{
	pdaStackCell stackCell[100];
	int sp;
	void init(){ sp = 0; }
	void push(char ch, int nr){ stackCell[sp].symb = ch; stackCell[sp].state = nr; sp++; }
	void pop(){ sp--; }
	pdaStackCell top(){ return stackCell[sp - 1]; }
};

struct prodCell{
	char symb;
	string transf;
	bool emit;
};

struct prod{
	prodCell production[100];
	int prodLen;
	void init(){ prodLen = 0; }
};

struct atributeStack{
	string str[100];
	int size;
	void init(){ size = 0; }
	void push(string s){ str[size++] = s; }
	void pop(){ size--; }
	string top(){ return str[size - 1]; }
};

string terminals;
string nonTerminals;
prod productions;
pdaStack pdastack;
atributeStack atributestack;
string TA[100][100];
string TS[100][100];
string Input;
string result[100];
int lineTa = 0, rowTa = 0, lineTs = 0, rowTs = 0, k=0;

int retIndexTerms(char ch) {
	size_t index = terminals.find(ch);
	if (index != string::npos)
		return index;
	return -1;
}

int retIndexNonTerms(char ch) {
	size_t index = nonTerminals.find(ch);
	if (index != string::npos)
		return index;
	return -1;
}

void main(){
	ifstream f;
	f.open("in.txt");
	productions.init();
	atributestack.init();
	pdastack.init();
	if (f.is_open()){
		if (!f.eof()){
			f >> nonTerminals;

			f >> terminals;

			f >> productions.prodLen;

			for (int i = 1; i <= productions.prodLen; i++){
				f >> productions.production[i].symb;
				f >> productions.production[i].transf;
				f >> productions.production[i].emit;
			}

			f >> lineTa;
			f >> rowTa;
			for (int i = 0; i < lineTa; i++) {
				for (int j = 0; j < rowTa; j++) {
					f >> TA[i][j];
				}
			}

			f >> lineTs;
			f >> rowTs;
			for (int i = 0; i < lineTs; i++) {
				for (int j = 0; j < rowTs; j++) {
					f >> TS[i][j];
				}
			}

			f >> Input;
		}
	}

	f.close();
	int val, val1, aux, lenghtR = 0;
	string val2 = "";
	pdastack.push('$', 0);
	while (!Input.empty()){
		val = pdastack.top().state;
		val1 = retIndexTerms(Input[0]);
		val2 = TA[val][val1];
		if (val2[0] == 'd'){
			val2 = val2.substr(1);
			pdastack.push(Input[0], atoi(val2.c_str()));
			Input = Input.substr(1);
		}
		else if (val2[0] == 'r'){
			aux = atoi(val2.substr(1).c_str());
			lenghtR = productions.production[aux].transf.length();
			for (int i = 1; i <= lenghtR; i++){
				if (pdastack.top().symb == 'a')
					atributestack.push("a");
				else if (retIndexTerms(pdastack.top().symb)){
					if (pdastack.top().symb == '+' || pdastack.top().symb == '*'){
						result[k] = atributestack.top() + pdastack.top().symb;
						atributestack.pop();
						result[k] += atributestack.top();
						atributestack.pop();
						atributestack.push("t");
						k++;
					}
				}
				pdastack.pop();
			}
			val = pdastack.top().state;
			pdastack.push(productions.production[aux].symb, atoi(TS[val][retIndexNonTerms(productions.production[aux].symb)].c_str()));
		}
		else if (val2 == "acc") {
			cout << "accept\n";
			break;
		}
		else {
			cout << "ERROR\n"; break;
		}
	}
	for (int i = 0; i < k; i++){
		cout << "t" << i << "=" << result[i] << endl;
	}
	system("PAUSE");
}